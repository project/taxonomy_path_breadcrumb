<?php

/**
 * @file
 * Contains taxonomy_path_breadcrumb.module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Implements hook_help().
 */
function taxonomy_path_breadcrumb_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the taxonomy_path_breadcrumb module.
    case 'help.page.taxonomy_path_breadcrumb':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Use Path breadcrumbs on taxonomy terms') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_form_FORM_ID_alter(). for taxonomy_vocabulary_form
 *
 * Third Party Setting for using custom breadcrumb builder for a vocabulary
 */
function taxonomy_path_breadcrumb_form_taxonomy_vocabulary_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form['taxonomy_path_breadcrumbs'] = [
    '#type' => 'details',
    '#title' => t('Breadcrumb builder settings'),
    '#group' => 'additional_settings'
  ];
  /* @var \Drupal\taxonomy\Entity\Vocabulary $vocabulary */
  $vocabulary = $form_state->getFormObject()->getEntity();

  $form['taxonomy_path_breadcrumbs']['taxonomy_path_breadcrumbs_builder'] = [
    '#type' => 'select',
    '#title' => t('Select Breadcrumb Service'),
    '#options' => [
      'taxonomy_term.breadcrumb' => t('Default'),
      'system.breadcrumb.default' => t('Path based, Drupal core')
    ],
    '#default_value' => $vocabulary->getThirdPartySetting('taxonomy_path_breadcrumb', 'taxonomy_path_breadcrumbs_builder')
  ];

  $form['#entity_builders'][] = 'taxonomy_path_breadcrumb_form_builder';
}

/**
 * setting the Third Party setting via entity builder
 *
 * @param $entity_type
 * @param \Drupal\taxonomy\Entity\Vocabulary $vocabulary
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 */
function taxonomy_path_breadcrumb_form_builder($entity_type, Vocabulary $vocabulary, &$form, FormStateInterface $form_state) {
  if ($form_state->getValue('taxonomy_path_breadcrumbs_builder')) {
    $vocabulary->setThirdPartySetting('taxonomy_path_breadcrumb' , 'taxonomy_path_breadcrumbs_builder', $form_state->getValue('taxonomy_path_breadcrumbs_builder'));
  }
}
