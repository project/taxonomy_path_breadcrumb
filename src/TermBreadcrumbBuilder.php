<?php

namespace Drupal\taxonomy_path_breadcrumb;

use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\TermInterface;

/**
 * Class TermBreadcrumbBuilder.
 */
class TermBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  /**
   * Constructs a new TermBreadcrumbBuilder object.
   */
  public function __construct() {
  }

  /**
   * {@inheritdoc}
   *
   * same as \Drupal\taxonomy\TermBreadcrumbBuilder
   */
  public function applies(RouteMatchInterface $route_match) {
    return $route_match->getRouteName() == 'entity.taxonomy_term.canonical'
      && $route_match->getParameter('taxonomy_term') instanceof TermInterface;
  }

  /**
   * {@inheritdoc}
   *
   * use different Breadcrumb Builder based on vocabulary setting
   */
  public function build(RouteMatchInterface $route_match) {
    /* @var \Drupal\taxonomy\Entity\Term $term */
    $term = $route_match->getParameter('taxonomy_term');
    /* @var \Drupal\taxonomy\Entity\Vocabulary $vocabulary */
    $vocabulary = Vocabulary::load($term->bundle());

    $buildersetting = $vocabulary->getThirdPartySetting('taxonomy_path_breadcrumb', 'taxonomy_path_breadcrumbs_builder');

    $breadcrumb_service = $buildersetting ? $buildersetting : 'taxonomy_term.breadcrumb';

    return \Drupal::service($breadcrumb_service)->build($route_match);
  }

}
